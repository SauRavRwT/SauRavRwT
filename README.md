<!-- My profile -->

> **"Turning creativity into code, imagination into reality, and complexity into simplicity, one line at a time."**

### Hi there 👋 , I'm [Saurav Rawat](https://sauravrwt.github.io)<div  align="right"><img src="https://komarev.com/ghpvc/?username=SauRavRwT&label=Visitors&color=64CCC5&style=flat" alt="visitor badge"/></div>

Here are some ideas to get you started:

- 🔭 I’m currently working on Team Jappy(SIH)...
- 🌱 I’m currently learning ReactJs...
- 👯 I’m looking to collaborate on Team Jappy(SIH)...
- 🤔 I’m looking for help with firebase...
- 💬 Ask me about Web Developing...
- 📫 How to reach me: [@balbheji](https://t.me/balbheji)
- ⚡ Fun fact: I'm Noob...

I'm Web Developer and UI/UX Designer from Delhi, India working in web development. My job is to build your website so that it is functional and user-friendly but at the same time attractive. Moreover, I add personal touch to your product and make sure that is eye-catching and easy to use. My aim is to bring across your message and identity in the most creative way.

### Github Stats

![Saurav's Github Stats](https://github-readme-stats.vercel.app/api?username=SauRavRwT&show_icons=true&theme=transparent&title_color=DAFFFB&text_color=176B87&icon_color=64CCC5&hide_border=true)![Saurav's Github Stats](https://github-readme-streak-stats.herokuapp.com?user=SauRavRwT&amp;hide_border=true&amp;dates=DAFFFB&amp;currStreakNum=176B87&amp;sideLabels=DAFFFB&amp;sideNums=64CCC5&amp;fire=176B87&amp;stroke=64CCC5&amp;ring=64CCC5&amp;background=FFFFFF00)
  
_I love connecting with different people so if you want to say **hi, I'll be happy to talk with you more** :)_

💙 If you like my projects, Give them ⭐ and Share it with friends!

**Made with ❤️ in India**